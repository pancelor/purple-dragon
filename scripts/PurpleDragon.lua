local dev,dev_z4,dev_invincible
-- dev=true
dev_z4=dev
dev_invincible=dev

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local confusion = require "necro.game.character.Confusion"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local player = require "necro.game.character.Player"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

local enemyPool = require "enemypool.EnemyPool"

components.register {
  marker={},
}

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    object.spawn("purpledragon_dragon",-2,0)
    -- object.spawn("Skeleton",-2,1)

    if dev_invincible then
      for _,p in ipairs(player.getPlayerEntities()) do
        invincibility.activate(p,999)
      end
    end
  end)
end
if dev_z4 then
  event.levelGenerate.add("generateProceduralLevel", {sequence=-1}, function (ev)
    -- all zones are zone 4
    local zone = 4
    ev.level = (ev.level%4) + (zone-1)*4
  end)
end

-- -- AOE field of confusion; needs some work
-- event.preAI.add("doPurpleDragon", {order="specialAction", filter="purpledragon_marker"}, function(ev)
--   for player in ecs.entitiesWithComponents{"playableCharacter","position"} do
--     local dx = math.abs(ev.entity.position.x - player.position.x)
--     local dy = math.abs(ev.entity.position.y - player.position.y)
--     if dx<=2 and dy<=2 then
--       dbg("activate")
--       confusion.inflict(player,2)
--     end
--   end
-- end)

event.objectTakeDamage.add("doPurpleDragon", {order="spell", filter="purpledragon_marker"}, function(ev)
  local dragon=ev.entity
  local player=ev.attacker
  if player and player:hasComponent"playableCharacter" and ev.type == damage.Type.PHYSICAL then
    -- -- random lol
    -- local dx = math.random(-2,2)
    -- local dy = math.random(-2,2)
    -- move.absolute(player,dragon.position.x+dx,dragon.position.y+dy,bit.bor(move.Flag.RESET_PREVIOUS_POSITION, move.Flag.VOCALIZE))

    -- deterministic
    local dx = dragon.position.x - player.position.x
    local dy = dragon.position.y - player.position.y
    dx,dy=action.rotate(dx,dy,action.Rotation.CW_90)
    move.absolute(player,dragon.position.x+dx,dragon.position.y+dy,bit.bor(move.Flag.RESET_PREVIOUS_POSITION, move.Flag.VOCALIZE))
    -- todo: don't tp/etc on killing blow

    -- todo: what to do if position is blocked? try next rotation? don't tp?
    object.moveToNearbyVacantTile(player)
    confusion.inflict(player,4)
    dragon.beatDelay.counter=1 --stun
    -- todo: play confusion sound
  end
end)

customEntities.extend {
  name = "dragon",
  template = customEntities.template.enemy("dragon",1),
  components = {
    friendlyName={name="Purple Dragon"},
    purpledragon_marker={},
    health={maxHealth=6, health=6},
    innateAttack={damage=6},
    sprite = {
      texture = "mods/purpledragon/images/purple-dragon.png",
    },
  },
}

event.levelLoad.add("registerPurpleDragon", {order="entities"}, function(ev)
  if currentLevel.getZone()==4 then
    enemyPool.registerConversion{from="Dragon2",to="purpledragon_dragon"}
    enemyPool.registerConversion{from="Dragon3",to="purpledragon_dragon"}
  end
end)
